FROM maven:3-eclipse-temurin-8-focal

COPY splitted-cacerts /usr/local/share/ca-certificates
COPY company /opt/company

RUN update-ca-certificates

LABEL decription="maven image enhanced with company certificates"

